import {apiRequest} from './apiConfig';
import {URL_API} from '../constants/api';

interface IParameters {
  [key: string]: string | number
}

export const api = {
  getAlbums: async () => apiRequest(`${URL_API}/albums`, 'GET'),
  getPhotos: async (params: IParameters) => {
    const parameters = Object.keys(params);
    const result = parameters.map((par) => `${par}=${params[par]}`).join('&');

    return apiRequest(`${URL_API}/photos?${result}`, 'GET')
  },
  getUsers: async () => apiRequest(`${URL_API}/users`, 'GET')
}
