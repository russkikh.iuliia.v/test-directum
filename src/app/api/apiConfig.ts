export const apiRequest = (url: string, method: string) => {
  return fetch(url, {
    method,
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
    }
  });
};
