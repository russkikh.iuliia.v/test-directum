import React from 'react';
import {MainLayout} from '../layouts/MainLayout/MainLayout';
import {GalleryContainer} from '../containers/GalleryContainer';

export const HomePage = () => {
  return (
    <MainLayout>
      <GalleryContainer/>
    </MainLayout>
  )
}
