import React from 'react';
import {useParams} from 'react-router-dom';
import {MainLayout} from '../layouts/MainLayout/MainLayout';
import {UserCardContainer} from '../containers/UserCardContainer';

export const UserPage = () => {
  const {userId} = useParams();
  return (
    <MainLayout>
      <UserCardContainer id={Number(userId)}/>
    </MainLayout>
  )
}
