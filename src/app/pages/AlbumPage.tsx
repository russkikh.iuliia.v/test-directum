import React from 'react';
import {useParams} from 'react-router-dom';
import {MainLayout} from '../layouts/MainLayout/MainLayout';
import {AlbumCardContainer} from '../containers/AlbumCardContainer';

export const AlbumPage = () => {
  const {albumId} = useParams();
  return (
    <MainLayout>
      <AlbumCardContainer id={Number(albumId)}/>
    </MainLayout>
  )
}
