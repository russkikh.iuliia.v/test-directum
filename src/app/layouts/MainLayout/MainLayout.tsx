import React from 'react';
import styles from './MainLayout.module.scss'

export const MainLayout = ({children}: { children: JSX.Element }) => (
  <div className={styles.Page}>
    <header className={styles.Header}>header</header>
    <main className={styles.Main}>
      <div className={styles.Wrapper}>
        <div className={styles.Container}>
          {children}
        </div>
      </div>
    </main>
    <footer className={styles.Footer}>footer</footer>
  </div>
)
