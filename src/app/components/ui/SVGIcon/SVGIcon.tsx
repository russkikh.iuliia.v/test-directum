import React from 'react'

interface ISVGIconProps {
  name: string
}

export const SVGIcon = (props: ISVGIconProps) => {
  const {
    name,
    ...rest
  } = props;
  return (
    <svg {...rest}>
      <use xlinkHref={`#${name}`}/>
    </svg>
  )
}
