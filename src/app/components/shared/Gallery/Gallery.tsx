import React, {LegacyRef} from 'react';
import {Loader} from '../../ui/Loader/Loader';
import styles from './Gallery.module.scss'

interface IGalleryProps {
  observerRef: LegacyRef<HTMLDivElement> | undefined,
  isLoading: boolean,
  children: JSX.Element
}

export const Gallery = (props: IGalleryProps) => {
  const {observerRef, isLoading, children} = props;
  return (
    <div className={styles.Gallery}>
      <div className={styles.Gallery__Photo}>
        {children}
      </div>
      {
        isLoading &&
        <div ref={observerRef} className={styles.Gallery__Loading}>
          <Loader/>
        </div>
      }
    </div>
  )
}
