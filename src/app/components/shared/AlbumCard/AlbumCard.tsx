import React from 'react';
import {IAlbum} from '../../../models/Album/model';
import {IPhoto} from '../../../models/Photo/model';
import {Loader} from '../../ui/Loader/Loader';
import {PhotoCard} from '../PhotoCard/PhotoCard';
import styles from './AlbumCard.module.scss';

interface IAlbumCardProps {
  album: IAlbum | undefined,
  loading: boolean,
  photos: IPhoto[]
}

export const AlbumCard = (props: IAlbumCardProps) => {
  const {album, loading, photos} = props;

  return (
    <div className={styles.AlbumCard}>
      {album &&
        <p className={styles.AlbumCard__Title}>{album.title}</p>
      }
      <div className={styles.AlbumCard__Photos}>
        {photos.map(photo => (
          <PhotoCard key={photo.id} photo={photo} shortCard={true}/>
        ))}
      </div>
      {loading &&
        <div className={styles.AlbumCard__Loading}>
          <Loader/>
        </div>
      }
    </div>
  )
}
