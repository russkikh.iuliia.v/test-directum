import React from 'react';
import {Link} from 'react-router-dom';
import {IUser} from '../../../models/User/model';
import {IAlbum} from '../../../models/Album/model';
import {SVGIcon} from '../../ui/SVGIcon/SVGIcon';
import styles from './UserCard.module.scss';

interface IUserCardProps {
  albums: IAlbum[],
  user: IUser | undefined
}

export const UserCard = (props: IUserCardProps) => {
  const {albums, user} = props;

  return (
    <div className={styles.UserCard}>
      <div className={styles.UserCard__Info}>
        {user &&
        <div className={styles.UserCard__User}>
          <p className={styles.UserCard__Name}>{user.name}</p>
          <div className={styles.UserCard__Contacts}>
            <a href={`mailto:${user.email}`} className={styles.UserCard__Contact}>
              <SVGIcon name='email'/>
              <span>{user.email}</span>
            </a>
            <a href={`tel:${user.phone}`} className={styles.UserCard__Contact}>
              <SVGIcon name='phone'/>
              <span>{user.phone}</span>
            </a>
            <a href={user.website} className={styles.UserCard__Contact}>
              <SVGIcon name='web'/>
              <span>{user.website}</span>
            </a>
          </div>
        </div>
        }
      </div>
      <div className={styles.UserCard__Albums}>
        {albums.map(al => (
          <Link to={`albums/${al.id}`} key={al.id} className={styles.UserCard__Album}>
            <span>{al.title}</span>
          </Link>
        ))}
      </div>
    </div>
  )
}
