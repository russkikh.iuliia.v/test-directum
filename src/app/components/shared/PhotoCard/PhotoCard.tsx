import React from 'react';
import {Link} from 'react-router-dom';
import {IPhoto} from '../../../models/Photo/model';
import {IUser} from '../../../models/User/model';
import styles from './PhotoCard.module.scss';

interface IPhotoCard {
  photo: IPhoto,
  shortCard: boolean,
  user?: IUser | undefined
}

export const PhotoCard = (props: IPhotoCard) => {
  const {photo, shortCard, user} = props;
  return (
    <div className={styles.PhotoCard}>
      <img className={styles.PhotoCard__Img} src={photo.thumbnailUrl} alt={photo.title}/>
      {(user && !shortCard) &&
        <Link className={styles.PhotoCard__Link} to={`users/${user.id}`}>{user.name}</Link>
      }
    </div>
  )
}
