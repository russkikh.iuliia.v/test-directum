import React, {useEffect, useState} from 'react';
import {useAppSelector} from '../hooks/hooks';
import {IPhoto} from '../models/Photo/model';
import {IUser} from '../models/User/model';
import {PhotoCard} from '../components/shared/PhotoCard/PhotoCard';

interface IPhotoCard {
  photo: IPhoto,
  shortCards?: boolean
}

export const PhotoCardContainer = (props: IPhotoCard) => {
  const {photo, shortCards = false} = props;

  const {albums, users} = useAppSelector(state => state.entities);
  const [currentUser, setCurrentUser] = useState<IUser>();

  useEffect(() => {
    const album = albums.find(al => al.id === photo.albumId);
    if (!album) return;
    setCurrentUser(users.find(us => us.id === album.userId));
  }, [albums, users]);

  return (
    <>
      <PhotoCard photo={photo} user={currentUser} shortCard={shortCards} />
    </>
  )
}
