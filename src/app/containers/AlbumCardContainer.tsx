import React, {useEffect, useState} from 'react';
import {useAppSelector} from '../hooks/hooks';
import {api} from '../api/api';
import {IAlbum} from '../models/Album/model';
import {IPhoto} from '../models/Photo/model';
import {AlbumCard} from '../components/shared/AlbumCard/AlbumCard';

interface IUserCardContainerProps {
  id: number
}

export const AlbumCardContainer = (props: IUserCardContainerProps) => {
  const {id} = props;

  const [album, setAlbum] = useState<IAlbum>();
  const [photos, setPhotos] = useState<IPhoto[]>([]);
  const [loading, setLoading] = useState(false);

  const {albums} = useAppSelector(state => state.entities);

  useEffect(() => {
    setAlbum(albums.find(al => al.id === id));
  }, [albums]);

  useEffect(() => {
    setLoading(true);
    api.getPhotos({albumId: id})
      .then(res => res.json())
      .then(data => setPhotos(data))
      .catch(error => console.log(error))
      .finally(() => setLoading(false));
  }, [id]);

  return (
    <AlbumCard album={album} photos={photos} loading={loading}/>
  )
}
