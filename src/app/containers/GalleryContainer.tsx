import React, {useEffect, useRef, useState} from 'react';
import {api} from '../api/api';
import {useAppDispatch, useAppSelector} from '../hooks/hooks';
import {setPhotos} from '../store/entities';
import {Gallery} from '../components/shared/Gallery/Gallery';
import {PhotoCardContainer} from './PhotoCardContainer';
import {setRequestPage} from '../store/requestPage';

export const GalleryContainer = () => {
  const [fetching, setFetching] = useState(false);
  const [firstRending, setFirstRending] = useState(true);
  const [totalCount, setTotalCount] = useState(0);

  const ref = useRef(null);

  const dispatch = useAppDispatch();
  const {photos} = useAppSelector(state => state.entities);
  const currentPage = useAppSelector(state => state.currentPage.requestPage)

  useEffect(() => {
    setFetching(true);
  }, []);

  useEffect(() => {
    if (!photos.length) return;

    const observer = new IntersectionObserver(
      ([entry], observer) => {
        if (entry.isIntersecting) {
          setFetching(true);
        }
      },
      {
        rootMargin: '0px 0px 200px 0px',
        threshold: 0
      }
    );

    if (ref.current) {
      observer.observe(ref.current);
    }

    return () => observer.disconnect();
  }, [ref.current, photos]);

  useEffect(() => {
    if (firstRending && currentPage !== 1) {
      setFetching(false);
      setFirstRending(false);
    }

    if (fetching) {
      api.getPhotos({_limit: 100, _page: currentPage})
        .then(res => {
          setTotalCount(Number(res.headers.get('x-total-count')))
          return res.json();
        })
        .then(data => {
          dispatch(setRequestPage(currentPage + 1));
          dispatch(setPhotos(data));
        })
        .catch(error => console.log(error))
        .finally(() => {
          setFetching(false);
          setFirstRending(false);
        });
    }
  }, [fetching]);

  return (
    <div>
      <Gallery observerRef={ref} isLoading={photos.length !== totalCount}>
        <>
          {photos.map((photo) => (
            <PhotoCardContainer key={photo.id} photo={photo}/>
          ))}
        </>
      </Gallery>
    </div>
  );
}

