import React, {useEffect, useState} from 'react';
import {useAppSelector} from '../hooks/hooks';
import {IUser} from '../models/User/model';
import {IAlbum} from '../models/Album/model';
import {UserCard} from '../components/shared/UserCard/UserCard';

interface IUserCardContainerProps {
  id: number
}

export const UserCardContainer = (props: IUserCardContainerProps) => {
  const {id} = props;

  const [user, setUser] = useState<IUser>();
  const [userAlbums, setUserAlbums] = useState<IAlbum[]>([]);
  const {albums, users} = useAppSelector(state => state.entities);

  useEffect(() => {
    setUser(users.find(us => us.id === id));
    setUserAlbums(albums.filter(al => al.userId === id));
  }, [albums, users]);
  return (
    <UserCard albums={userAlbums} user={user}/>
  )
}
