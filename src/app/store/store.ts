import {configureStore, ThunkAction, Action} from '@reduxjs/toolkit';
import {entitiesSlice} from './entities';
import {requestPageSlice} from './requestPage';

export const store = configureStore({
  reducer: {
    entities: entitiesSlice.reducer,
    currentPage: requestPageSlice.reducer
  },
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<ReturnType,
  RootState,
  unknown,
  Action<string>>;
