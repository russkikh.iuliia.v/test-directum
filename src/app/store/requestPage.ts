import {createSlice} from '@reduxjs/toolkit';

const initialState = {
  requestPage: 1
};

export const requestPageSlice = createSlice({
  name: 'requestPage',
  initialState,
  reducers: {
    setRequestPage(state, action) {
      return {
        ...state,
        requestPage: action.payload
      }
    },
  }
});

export const {setRequestPage} = requestPageSlice.actions;
