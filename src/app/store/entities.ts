import {createSlice} from '@reduxjs/toolkit';
import {IAlbum} from '../models/Album/model'
import {IPhoto} from '../models/Photo/model';
import {IUser} from '../models/User/model';

interface IEntities {
  albums: IAlbum[],
  photos: IPhoto[],
  users: IUser[]
}

const initialState: IEntities = {
  albums: [],
  photos: [],
  users: []
};

export const entitiesSlice = createSlice({
  name: 'entities',
  initialState,
  reducers: {
    clearPhotos(state) {
      return {
        ...state,
        photos: []
      }
    },
    setAlbums(state, action) {
      return {
        ...state,
        albums: action.payload
      }
    },
    setPhotos(state, action) {
      return {
        ...state,
        photos: [...state.photos, ...action.payload]
      }
    },
    setUsers(state, action) {
      return {
        ...state,
        users: action.payload
      }
    },
  }
});

export const {clearPhotos, setAlbums, setPhotos, setUsers} = entitiesSlice.actions;
