import React, {useEffect} from 'react';
import {
  BrowserRouter as Router,
  Routes,
  Route,
} from 'react-router-dom';
import {useAppDispatch, useAppSelector} from './app/hooks/hooks';
import {setAlbums, setUsers} from './app/store/entities';
import {api} from './app/api/api';
import {SVGSprite} from './app/components/ui/SVGSprite/SVGSprite';
import {HomePage} from './app/pages/HomePage';
import {UserPage} from './app/pages/UserPage';
import {AlbumPage} from './app/pages/AlbumPage';
import './app/styles/general.scss';

export const App = () => {
  const {albums, users} = useAppSelector(state => state.entities);
  const dispatch = useAppDispatch();

  useEffect(() => {
    if (!albums.length || !users.length) {
      Promise.all([api.getAlbums(), api.getUsers()])
        .then(responses => Promise.all(responses.map(res => res.json())))
        .then(entities => {
          dispatch(setAlbums(entities[0]));
          dispatch(setUsers(entities[1]));
        })
        .catch(error => console.log(error));
    }
  }, []);

  return (
    <div className="App">
      <Router>
        <Routes>
          <Route path='/' element={<HomePage/>}/>
          <Route path='users/:userId' element={<UserPage/>}/>
          <Route path='users/:userId/albums/:albumId' element={<AlbumPage/>}/>
        </Routes>
      </Router>
      <SVGSprite/>
    </div>
  );
}
